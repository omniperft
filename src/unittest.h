#ifndef UNITTEST_H
#define UNITTEST_H

#include "chess.h"
#include "omniperft.h"


/*!
 * \brief Unit tests for Chess::Board.
 */
namespace UnitTest
{
	/*!
	* A minimax tree unit test for the Chess::Board class.
	*
	* Perft tests the correctness and performance of move generation,
	* makeMove and undoMove routines, and the Zobrist keys.
	*
	* \param board A Chess::Board object.
	* \param depth Search to game tree this many plies deep.
	* \param divide If true, a node count for each root move is shown.
	* \param threadCount The number of threads to use. If 0, perft will
	* use as many threads as there are available CPUs or cores.
	*
	* \return Number of nodes in a game tree that reaches \a depth plies.
	*/
	uint64_t perft(Chess::Board& board, int depth, bool divide, int threadCount = 0);
}

#endif // UNITTEST
