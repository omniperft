/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cassert>
#include "chessboard.h"
#include "notation.h"
#include "zobrist.h"

using namespace Chess;


Board::Board(Variant variant)
: m_variant(variant), m_isRandom(false)
{
	switch (m_variant) {
	case StandardChess:
		m_width = 8;
		m_height = 8;
		break;
	case CapablancaChess:
		m_width = 10;
		m_height = 8;
		break;
	}
	
	// Allocate the squares, with one 'wall' file on both sides,
	// and two 'wall' ranks at the top and bottom.
	m_arwidth = m_width + 2;
	m_squares.resize(m_arwidth * (m_height + 4), InvalidPiece);
	
	
	// Initialize the move offsets
	
	m_castleTarget[White][QueenSide] = (m_height + 1) * m_arwidth + 3;
	m_castleTarget[White][KingSide] = (m_height + 1) * m_arwidth + m_width - 1;
	m_castleTarget[Black][QueenSide] = 2 * m_arwidth + 3;
	m_castleTarget[Black][KingSide] = 2 * m_arwidth + m_width - 1;
	
	m_knightOffsets.resize(8);
	m_knightOffsets[0] = -2 * m_arwidth - 1;
	m_knightOffsets[1] = -2 * m_arwidth + 1;
	m_knightOffsets[2] = -m_arwidth - 2;
	m_knightOffsets[3] = -m_arwidth + 2;
	m_knightOffsets[4] = m_arwidth - 2;
	m_knightOffsets[5] = m_arwidth + 2;
	m_knightOffsets[6] = 2 * m_arwidth - 1;
	m_knightOffsets[7] = 2 * m_arwidth + 1;
	
	m_bishopOffsets.resize(4);
	m_bishopOffsets[0] = -m_arwidth - 1;
	m_bishopOffsets[1] = -m_arwidth + 1;
	m_bishopOffsets[2] = m_arwidth - 1;
	m_bishopOffsets[3] = m_arwidth + 1;
	
	m_rookOffsets.resize(4);
	m_rookOffsets[0] = -m_arwidth;
	m_rookOffsets[1] = -1;
	m_rookOffsets[2] = 1;
	m_rookOffsets[3] = m_arwidth;
}

void Board::initZobristKey()
{
	m_key = 0;
	
	for (int side = White; side <= Black; side++) {
		const int* rookSq = m_castlingRights.rookSquare[side];
		if (rookSq[QueenSide] != 0)
			m_key ^= Zobrist::castling(side, rookSq[QueenSide]);
		if (rookSq[KingSide] != 0)
			m_key ^= Zobrist::castling(side, rookSq[KingSide]);
		
		int sign = (side == White) ? 1 : -1;
		for (unsigned sq = 0; sq < m_squares.size(); sq++) {
			int piece = m_squares[sq];
			if (piece == InvalidPiece || piece * sign <= 0)
				continue;
			
			m_key ^= Zobrist::piece(side, piece * sign, sq);
		}
	}
	
	if (m_enpassantSquare != 0)
		m_key ^= Zobrist::enpassant(m_enpassantSquare);
	if (m_side == Black)
		m_key ^= Zobrist::side();
}

Variant Board::variant() const
{
	return m_variant;
}

uint64_t Board::key() const
{
	return m_key;
}

void Board::print() const
{
	int i = m_arwidth * 2;
	for (int y = 0; y < m_height; y++) {
		i++;
		for (int x = 0; x < m_width; x++) {
			int pc = m_squares[i];
			char c = '.';
			
			if (pc != NoPiece)
				c = Notation::pieceChar(pc);
			std::cout << c << " ";
			
			i++;
		}
		i++;
		std::cout << std::endl;
	}
	std::cout << "FEN: " << fenString() << std::endl;
}

void Board::printMoves(Chess::MoveNotation notation)
{
	std::vector<Move> moves = legalMoves();
	std::vector<Move>::iterator it;
	for (it = moves.begin(); it != moves.end(); ++it)
		std::cout << moveString(*it, notation) << std::endl;
}

Square Board::chessSquare(int index) const
{
	int file = (index % m_arwidth) - 1;
	int rank = (m_height - 1) - ((index / m_arwidth) - 2);
	Square square = { file, rank };
	
	return square;
}

int Board::squareIndex(const Square& square) const
{
	if (square.file < 0 || square.file >= m_width
	||  square.rank < 0 || square.rank >= m_height)
		return 0;
	
	int rank = (m_height - 1) - square.rank;
	return (rank + 2) * m_arwidth + 1 + square.file;
}

bool Board::isValidSquare(const Chess::Square& square) const
{
	if (square.file < 0 || square.file >= m_width
	||  square.rank < 0 || square.rank >= m_height)
		return false;
	return true;
}

bool Board::inCheck(int side, int square) const
{
	if (square == 0)
		square = m_kingSquare[side];
	
	int sign = (side == White) ? 1 : -1;
	int attacker;
	
	// Pawn attacks
	int step = -sign * m_arwidth;
	// Left side
	attacker  = m_squares[square + step - 1];
	if ((attacker * sign) == -Pawn)
		return true;
	// Right side
	attacker = m_squares[square + step + 1];
	if ((attacker * sign) == -Pawn)
		return true;
	
	std::vector<int>::const_iterator it;
	
	// Knight, archbishop, chancellor attacks
	for (it = m_knightOffsets.begin(); it != m_knightOffsets.end(); ++it) {
		attacker = m_squares[square + *it];
		switch (attacker * sign) {
		case -Knight: case -Archbishop: case -Chancellor:
			return true;
		}
	}
	
	// Bishop, queen, archbishop, king attacks
	for (it = m_bishopOffsets.begin(); it != m_bishopOffsets.end(); ++it) {
		int targetSquare = square + *it;
		if (targetSquare == m_kingSquare[!side])
			return true;
		while ((attacker = m_squares[targetSquare]) != InvalidPiece
		&&      attacker * sign <= 0) {
			switch (attacker * sign) {
			case -Bishop: case -Queen: case -Archbishop:
				return true;
			}
			if (attacker != NoPiece)
				break;
			targetSquare += *it;
		}
	}
	
	// Rook, queen, chancellor, king attacks
	for (it = m_rookOffsets.begin(); it != m_rookOffsets.end(); ++it) {
		int targetSquare = square + *it;
		if (targetSquare == m_kingSquare[!side])
			return true;
		while ((attacker = m_squares[targetSquare]) != InvalidPiece
		&&      attacker * sign <= 0) {
			switch (attacker * sign) {
			case -Rook: case -Queen: case -Chancellor:
				return true;
			}
			if (attacker != NoPiece)
				break;
			targetSquare += *it;
		}
	}
	
	return false;
}

void Board::makeMove(const Move& move)
{
	int source = move.sourceSquare();
	int target = move.targetSquare();
	int promotion = move.promotion();
	int piece = m_squares[source] * m_sign;
	int capture = m_squares[target];
	int epSq = m_enpassantSquare;
	int* rookSq = m_castlingRights.rookSquare[m_side];
	
	assert(source != 0);
	assert(target != 0);
	
	MoveData md = { move, capture, epSq, m_castlingRights,
	                m_key, m_reversibleMoveCount };
	
	m_key ^= Zobrist::piece(m_side, piece, source);
	
	if (epSq != 0) {
		m_key ^= Zobrist::enpassant(epSq);
		m_enpassantSquare = 0;
	}
	
	bool isReversible = true;
	bool clearSource = true;
	if (piece == King) {
		m_kingSquare[m_side] = target;
		
		// In case of a castling move, make the rook's move
		if (move.castlingSide() != -1) {
			int cside = move.castlingSide();
			int rsource = rookSq[cside];
			int rtarget = (cside == QueenSide) ? target + 1 : target -1;
			
			if ((rtarget == source) || (target == source))
				clearSource = false;
			m_squares[rsource] = NoPiece;
			m_squares[rtarget] = Rook * m_sign;
			m_key ^= Zobrist::piece(m_side, Rook, rsource);
			m_key ^= Zobrist::piece(m_side, Rook, rtarget);
			isReversible = false;
		}
		// Any king move removes all castling rights
		for (int i = QueenSide; i <= KingSide; i++) {
			int& rs = rookSq[i];
			if (rs != 0) {
				m_key ^= Zobrist::castling(m_side, rs);
				rs = 0;
			}
		}
	} else if (piece == Pawn) {
		isReversible = false;
		
		// Make an en-passant capture
		if (target == epSq) {
			int epTarget = target + m_arwidth * m_sign;
			m_squares[epTarget] = NoPiece;
			m_key ^= Zobrist::piece(!m_side, Pawn, epTarget);
		// Push a pawn two squares ahead, creating an en-passant
		// opportunity for the opponent.
		} else if ((source - target) * m_sign == m_arwidth * 2) {
			m_enpassantSquare = source - m_arwidth * m_sign;
			m_key ^= Zobrist::enpassant(m_enpassantSquare);
		} else if (promotion != NoPiece)
			piece = promotion;
	} else if (piece == Rook) {
		// Remove castling rights from the rook's square
		for (int i = QueenSide; i <= KingSide; i++) {
			if (source == rookSq[i]) {
				m_key ^= Zobrist::castling(m_side, source);
				rookSq[i] = 0;
				isReversible = false;
				break;
			}
		}
	}
	
	capture *= m_sign;
	// If the move captures opponent's castling rook, remove
	// his castling rights from that side.
	if (capture == -Rook) {
		int* opCr = m_castlingRights.rookSquare[!m_side];
		if (target == opCr[QueenSide]) {
			m_key ^= Zobrist::castling(!m_side, target);
			opCr[QueenSide] = 0;
		} else if (target == opCr[KingSide]) {
			m_key ^= Zobrist::castling(!m_side, target);
			opCr[KingSide] = 0;
		}
	}
	
	if (capture < 0) {
		isReversible = false;
		m_key ^= Zobrist::piece(!m_side, -capture, target);
	}
	m_key ^= Zobrist::side();
	m_key ^= Zobrist::piece(m_side, piece, target);
	m_squares[target] = piece * m_sign;
	if (clearSource)
		m_squares[source] = NoPiece;
	
	if (isReversible)
		m_reversibleMoveCount++;
	else
		m_reversibleMoveCount = 0;
	
	m_history.push_back(md);
	m_sign *= -1;
	m_side = !m_side;
}

void Board::undoMove()
{
	if (m_history.empty())
		return;
	
	const MoveData& md = m_history.back();
	const Move& move = md.move;
	int target = move.targetSquare();
	int source = move.sourceSquare();
	
	m_history.pop_back();
	m_sign *= -1;
	m_side = !m_side;
	
	m_enpassantSquare = md.enpassantSquare;
	m_castlingRights = md.castlingRights;
	m_key = md.key;
	m_reversibleMoveCount = md.reversibleMoveCount;
	
	if (target == m_kingSquare[m_side]) {
		m_kingSquare[m_side] = source;
		
		int cside = move.castlingSide();
		if (cside != -1) {
			// Move the rook back after castling
			if (cside == QueenSide)
				m_squares[target + 1] = NoPiece;
			else
				m_squares[target - 1] = NoPiece;
			const int* cr = m_castlingRights.rookSquare[m_side];
			m_squares[cr[cside]] = Rook * m_sign;
			m_squares[source] = King * m_sign;
			m_squares[target] = md.capture;
			return;
		}
	} else if (target == m_enpassantSquare) {
		// Restore the pawn captured by the en-passant move
		int epTarget = target + m_arwidth * m_sign;
		m_squares[epTarget] = -Pawn * m_sign;
	}
	
	if (move.promotion() != NoPiece)
		m_squares[source] = Pawn * m_sign;
	else
		m_squares[source] = m_squares[target];
	
	m_squares[target] = md.capture;
}

bool Board::isLegalPosition() const
{
	if (inCheck(!m_side))
		return false;
	
	if (m_history.empty())
		return true;
	
	const MoveData& md = m_history.back();
	const Move& move = md.move;
	
	// Make sure that no square between the king's initial and final
	// squares (including the initial and final squares) are under
	// attack (in check) by the opponent.
	if (move.castlingSide() != -1) {
		int source = move.sourceSquare();
		int target = move.targetSquare();
		int offset = (move.castlingSide() == KingSide) ? 1 : -1;

		if (source == target) {
			int i = target - offset;
			while (true) {
				i -= offset;
				int piece = m_squares[i];
				if (piece == InvalidPiece)
					return true;
				switch (piece * m_sign) {
				case Rook: case Queen: case Chancellor:
					return false;
				}
			}
		}

		for (int i = source; i != target; i += offset) {
			if (inCheck(!m_side, i))
				return false;
		}
	}
	
	return true;
}

bool Board::isLegalMove(const Chess::Move& move)
{
	std::vector<Move> moves = legalMoves();
	std::vector<Move>::const_iterator it;
	for (it = moves.begin(); it != moves.end(); it++) {
		if (it->sourceSquare() == move.sourceSquare()
		&&  it->targetSquare() == move.targetSquare()
		&&  it->promotion() == move.promotion()
		&&  it->castlingSide() == move.castlingSide())
			return true;
	}
	
	return false;
}
