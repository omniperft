#ifndef ZOBRIST_H
#define ZOBRIST_H

#include "omniperft.h"

/*!
 * \brief Unsigned 64-bit values for generating zobrist position keys.
 *
 * Chess::Board uses zobrist keys to quickly and easily compare two
 * positions for equality. Primary uses for zobrist keys are:
 * - Detecting repetitions, ie. a draw by three-fold repetition
 * - Opening books
 * - In hash table entries
 *
 * \note This class is automatically initialized by a static
 * initializer object, so users should try to initialize it.
 */
class Zobrist
{
	public:
		/*!
		 * Returns the zobrist value for side to move.
		 * This value must be in the key on black's turn.
		 */
		static uint64_t side();
		
		/*!
		 * Returns the zobrist value for en-passant target
		 * at \a square.
		 */
		static uint64_t enpassant(int square);
		
		/*!
		 * Returns the zobrist value for player \a side's
		 * castling rook at \a square.
		 */
		static uint64_t castling(int side, int square);
		
		/*!
		 * Returns the zobrist value for player \a side's
		 * \a type piece at \a square.
		 */
		static uint64_t piece(int side, int type, int square);
		
	private:
		friend class ZobristInitializer;
		static const int MaxSquares = 144;
		static const int MaxPieceTypes = 16;
		
		Zobrist();
		
		/*! Initializes all zobrist components. */
		static void initialize();
		
		/*!
		 * The "minimal standard" random number generator
		 * by Park and Miller.
		 * Returns a pseudo-random integer between 1 and 2147483646.
		 */
		static int random();
		
		/*! Returns an unsigned 64-bit pseudo-random number. */
		static uint64_t random64();
		
		static int m_randomSeed;
		static bool m_isInitialized;
		
		static uint64_t m_side;
		static uint64_t m_enpassant[MaxSquares];
		static uint64_t m_castling[2][MaxSquares];
		static uint64_t m_piece[2][MaxPieceTypes][MaxSquares];
};

#endif // ZOBRIST
