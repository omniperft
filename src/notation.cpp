/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cctype>
#include <cstdlib>
#include "chessboard.h"
#include "notation.h"

using namespace Chess;


namespace {

char fileChar(int file)
{
	return 'a' + file;
}

char rankChar(int rank)
{
	return '1' + rank;
}

int fileInt(char c)
{
	return c - 'a';
}

int rankInt(char c)
{
	return c - '1';
}

} // unnamed namespace


namespace Notation {

char pieceChar(int pieceCode)
{
	char c;
	
	switch (abs(pieceCode)) {
	case Pawn:
		c = 'P';
		break;
	case Knight:
		c = 'N';
		break;
	case Bishop:
		c = 'B';
		break;
	case Rook:
		c = 'R';
		break;
	case Queen:
		c = 'Q';
		break;
	case King:
		c = 'K';
		break;
	case Archbishop:
		c = 'A';
		break;
	case Chancellor:
		c = 'C';
		break;
	default:
		return 0;
	}
	
	if (pieceCode < 0)
		return tolower(c);
	return c;
}

int pieceCode(char pieceChar)
{
	int p;
	
	switch (toupper(pieceChar)) {
	case 'P':
		p = Pawn;
		break;
	case 'N':
		p = Knight;
		break;
	case 'B':
		p = Bishop;
		break;
	case 'R':
		p = Rook;
		break;
	case 'Q':
		p = Queen;
		break;
	case 'K':
		p = King;
		break;
	case 'A':
		p = Archbishop;
		break;
	case 'C':
		p = Chancellor;
		break;
	default:
		return NoPiece;
	}
	
	if (islower(pieceChar))
		return -p;
	return p;
}

std::string squareString(const Square& square)
{
	std::string str;
	str += fileChar(square.file);
	str += rankChar(square.rank);
	return str;
}

Square square(const std::string& str)
{
	Square sq = { -1, -1 };
	if (str.length() != 2)
		return sq;
	
	sq.file = fileInt(str[0]);
	sq.rank = rankInt(str[1]);
	
	return sq;
}

} // namespace Notation
