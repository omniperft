/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include "zobrist.h"
#include "chess.h"

int Zobrist::m_randomSeed = 1;
bool Zobrist::m_isInitialized = false;
uint64_t Zobrist::m_side;
uint64_t Zobrist::m_enpassant[MaxSquares];
uint64_t Zobrist::m_castling[2][MaxSquares];
uint64_t Zobrist::m_piece[2][MaxPieceTypes][MaxSquares];


/*! \cond ZobristInitializer. */
class ZobristInitializer
{
	public:
		ZobristInitializer()
		{
			Zobrist::initialize();
		}
};
/*! \endcond ZobristInitializer. */
static ZobristInitializer zobristInitializer;


int Zobrist::random()
{
	const int a = 16807;
	const int m = 2147483647;
	const int q = (m / a);
	const int r = (m % a);

	int hi = m_randomSeed / q;
	int lo = m_randomSeed % q;
	int test = a * lo - r * hi;

	if (test > 0)
		m_randomSeed = test;
	else
		m_randomSeed = test + m;

	return m_randomSeed;
}

uint64_t Zobrist::random64()
{
	uint64_t random1 = (uint64_t)random();
	uint64_t random2 = (uint64_t)random();
	uint64_t random3 = (uint64_t)random();

	return random1 ^ (random2 << 31) ^ (random3 << 62);
}

uint64_t Zobrist::side()
{
	return m_side;
}

uint64_t Zobrist::enpassant(int square)
{
	assert(square >= 0 && square < MaxSquares);
	
	return m_enpassant[square];
}

uint64_t Zobrist::castling(int side, int square)
{
	assert(side != -1);
	assert(square >= 0 && square < MaxSquares);
	
	return m_castling[side][square];
}

uint64_t Zobrist::piece(int side, int type, int square)
{
	assert(side != -1);
	assert(type > 0);
	assert(square >= 0 && square < MaxSquares);
	
	return m_piece[side][type][square];
}

void Zobrist::initialize()
{
	if (m_isInitialized)
		return;
	
	int side;
	int square;
	
	m_side = random64();
	for (side = 0; side < 2; side++) {
		for (square = 0; square < MaxSquares; square++) {
			m_castling[side][square] = random64();
			
			for (int piece = 0; piece < MaxPieceTypes; piece++) {
				m_piece[side][piece][square] = random64();
			}
		}
	}
	for (square = 0; square < MaxSquares; square++)
		m_enpassant[square] = random64();
	
	m_isInitialized = true;
}
