/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "thread.h"


int Thread::cpuCount()
{
#if defined(_SC_NPROCESSORS_ONLN)	// Sun Solaris, Digital UNIX
	return sysconf(_SC_NPROCESSORS_ONLN);
#elif defined(_SC_NPROC_ONLN)		// Silicon Graphics IRIX
	return sysconf(_SC_NPROC_ONLN);
#elif defined(_SC_NPROCESSORS_CONF)	// favor _ONLN over _CONF
	return sysconf(_SC_NPROCESSORS_CONF);
#elif defined(WINDOWS)			// Microsoft Windows
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	return info.dwNumberOfProcessors;
#else
	return 0;
#endif
}


Thread::Thread(Thread::func_ptr func, void* arg)
{
#ifdef WINDOWS
	DWORD iID;
	m_handle = CreateThread(0, 0, func, arg, 0, &iID);
	if (!m_handle)
		std::cerr << "Thread creation failed" << std::endl;
#else
	int ret = pthread_create(&m_handle, 0, func, arg);
	if (ret != 0) {
		std::cerr << "Thread creation failed" << std::endl;
		m_handle = 0;
	}
#endif
}

Thread::~Thread()
{
	join();
}

void Thread::join()
{
	if (!m_handle)
		return;

#ifdef WINDOWS
	DWORD ret = WaitForSingleObject(m_handle, INFINITE);
	if (ret == WAIT_FAILED)
		std::cerr << "Join failed" << std::endl;
	CloseHandle(m_handle);
#else
	int ret = pthread_join(m_handle, 0);
	if (ret != 0)
		std::cerr << "Join failed" << std::endl;
#endif

	m_handle = 0;
}


Mutex::Mutex()
{
#ifdef WINDOWS
	InitializeCriticalSection(&m_mutex);
#else
	pthread_mutex_init(&m_mutex, 0);
#endif
}

Mutex::~Mutex()
{
#ifdef WINDOWS
	DeleteCriticalSection(&m_mutex);
#else
	pthread_mutex_destroy(&m_mutex);
#endif
}

void Mutex::lock()
{
#ifdef WINDOWS
	EnterCriticalSection(&m_mutex);
#else
	pthread_mutex_lock(&m_mutex);
#endif
}

void Mutex::unlock()
{
#ifdef WINDOWS
	LeaveCriticalSection(&m_mutex);
#else
	pthread_mutex_unlock(&m_mutex);
#endif
}


MutexLocker::MutexLocker(Mutex& mutex)
: m_mutex(mutex)
{
	m_mutex.lock();
}

MutexLocker::~MutexLocker()
{
	m_mutex.unlock();
}


ThreadGroup::ThreadGroup()
{

}

ThreadGroup::~ThreadGroup()
{
	joinAll();
}

Thread* ThreadGroup::create(Thread::func_ptr func, void* arg)
{
	Thread* t = new Thread(func, arg);
	m_threads.push_back(t);
	return t;
}

void ThreadGroup::joinAll()
{
	while (!m_threads.empty())
	{
		m_threads.back()->join();
		delete m_threads.back();

		m_threads.pop_back();
	}
}
