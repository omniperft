/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include "chessboard.h"
#include "unittest.h"


static bool tryFen(Chess::Board& board, const std::string& fen)
{
	if (board.setBoard(fen))
		return true;
	
	// If the fen string didn't work with the current variant,
	// change the variant and try again
	Chess::Variant variant = board.variant();
	if (variant == Chess::StandardChess)
		variant = Chess::CapablancaChess;
	else
		variant = Chess::StandardChess;
	
	board = Chess::Board(variant);
	return board.setBoard(fen);
}

static int readInput(Chess::Board& board)
{
	std::string str;
	std::string command;
	std::string param;
	
	if (!getline(std::cin, str))
		return 0;
	
	{
		std::istringstream ss(str);
		getline(ss, command, ' ');
		if (!getline(ss, param))
			param.clear();
	}
	
	if (command == "quit")
		return 1;
	if (command == "help") {
		std::cout << "Available commands:\n\n"
		          << "moves:\t\tPrint a list of legal moves\n"
		          << "perft [d]:\tPrint a perft value to depth [d]\n"
		          << "divide [d]:\tPerft to [d] with a separate count for each move\n"
		          << "printboard:\tPrint an ASCII board and the FEN/X-FEN string\n"
		          << "quit:\t\tExit the program\n"
		          << "undo:\t\tReverse the last move\n"
		          << "setboard [fen]:\tSet the board according to a FEN/X-FEN string\n"
		          << "[move]:\t\tMake [move] on the board. Accepts SAN and Coordinate notation"
		          << std::endl;
	} else if (command == "setboard") {
		if (!tryFen(board, param))
			std::cout << "Invalid FEN string" << std::endl;
		else
			board.print();
	} else if (command == "perft" || command == "divide") {
		int divide = (command == "divide") ? true : false;
		int depth;
		std::istringstream ss(param);
		if ((ss >> depth)) {
			time_t start, end;
			
			time(&start);
			std::cout << UnitTest::perft(board, depth, divide) << std::endl;
			time(&end);
			
			std::cout << "Elapsed time: " << difftime(end, start)
			          << " second(s)" << std::endl;
		}
	} else if (command == "printboard") {
		board.print();
	} else if (command == "moves") {
		board.printMoves(Chess::LongAlgebraic);
	} else if (command == "undo") {
		board.undoMove();
	} else {
		Chess::Move move = board.moveFromString(command);
		if (!board.isLegalMove(move))
			std::cout << "Illegal move" << std::endl;
		else
			board.makeMove(move);
	}
	
	return 0;
}

int main()
{
	std::cout << "OmniPerft 1.0 by Ilari Pihlajisto\n" << std::endl;
	
	Chess::Board board(Chess::StandardChess);
	board.setBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KkQq -");
	
	board.print();
	while (!readInput(board)) ;
	
	return 0;
}
