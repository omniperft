/*
    This file is part of OmniPerft.
    Copyright (C) 2008 Ilari Pihlajisto

    OmniPerft is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OmniPerft is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OmniPerft.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <iostream>
#include <cassert>
#include "unittest.h"
#include "chessboard.h"
#include "thread.h"

using Chess::Board;
using Chess::Move;


namespace {

/*!
 * \brief Struct for perft() hash table entries.
 *
 * To verify that the Zobrist keys work correctly, they are
 * used in a hash table in perft.
 */
struct PerftHash
{
	//! Search depth.
	int depth;
	//! The number of nodes in this sub-tree.
	uint64_t nodeCount;
	//! Zobrist key for the position.
	uint64_t key;
};

typedef std::vector<PerftHash> HashTable;

/*!
 * \brief Storage for the shared data used in perft().
 *
 * In perft(), this data is initialized by the main thread, and
 * then used (often simultaneously) by all worker threads.
 */
struct PerftData
{
	//! The workers increment this number after each root move.
	uint64_t nodeCount;
	//! Search depth for the workers.
	int depth;
	//! If true, a node count for each root move is shown.
	bool divide;
	//! The Board objects (one for each job/root move).
	std::vector<Board> boards;
	//! The root moves.
	std::vector<Move> moves;
	//! Hash table.
	HashTable hashTable;
	//! Mutex to serialize access to the jobs.
	Mutex jobMutex;
	//! Mutex to serialize access to the shared node count.
	Mutex nodeCountMutex;
	//! Vector index for the next job/root move in line.
	unsigned jobIndex;
};


uint64_t probePerftHash(uint64_t key,
                        int depth,
                        const HashTable& hashTable)
{
	const PerftHash& entry = hashTable[key % hashTable.size()];
	if (entry.key == key && entry.depth == depth)
		return entry.nodeCount;

	return 0;
}

void storePerftHash(uint64_t key,
                    uint64_t nodeCount,
                    int depth,
                    HashTable& hashTable)
{
	PerftHash& entry = hashTable[key % hashTable.size()];

	if (depth >= entry.depth) {
		entry.depth = depth;
		entry.key = key;
		entry.nodeCount = nodeCount;
	}
}

uint64_t perftSearch(Board& board, int depth, HashTable& hashTable)
{
	assert(depth > 0);
	
	uint64_t nodeCount;
	if (depth > 1) {
		nodeCount = probePerftHash(board.key(), depth, hashTable);
		if (nodeCount > 0)
			return nodeCount;
	}
	
	std::vector<Move> moves = board.legalMoves();
	if (depth == 1)
		return moves.size();
	
	nodeCount = 0;
	std::vector<Move>::const_iterator move;
	for (move = moves.begin(); move != moves.end(); ++move) {
		board.makeMove(*move);
		nodeCount += perftSearch(board, depth - 1, hashTable);
		board.undoMove();
	}
	
	if (depth > 1)
		storePerftHash(board.key(), nodeCount, depth, hashTable);
	
	return nodeCount;
}

tfunc_t threadFunc(void* data)
{
	PerftData& pd = *((PerftData*)data);
	
	while (true) {
		int i;
		{
			// Grab a job, or break if they're all taken
			MutexLocker jobLock(pd.jobMutex);
			if (pd.jobIndex >= pd.boards.size())
				break;
			i = pd.jobIndex++;
		}
		
		Board& board = pd.boards[i];
		const Move& move = pd.moves[i];
		std::string str = board.moveString(move, Chess::LongAlgebraic);
		
		board.makeMove(move);
		uint64_t nodeCount = perftSearch(board, pd.depth, pd.hashTable);
		
		// Update shared node count
		MutexLocker ncLock(pd.nodeCountMutex);
		if (pd.divide)
			std::cout << str << ": " << nodeCount << std::endl;
		pd.nodeCount += nodeCount;
	}
	
	return 0;
}

} // unnamed namespace


namespace UnitTest {

uint64_t perft(Board& board, int depth, bool divide, int threadCount)
{
	if (depth <= 0)
		return 0;
	if (depth == 1)
		return board.legalMoves().size();
	
	PerftData pd;
	pd.divide = divide;
	pd.nodeCount = 0;
	pd.depth = depth - 1;
	pd.jobIndex = 0;
	
	if (threadCount <= 0)
		threadCount = Thread::cpuCount();
	std::cout << "Perft with " << threadCount << " thread(s)" << std::endl;
	
	pd.moves = board.legalMoves();
	pd.boards = std::vector<Board>(pd.moves.size(), board);
	
	const int hashSize = 0x200000;
	PerftHash tmp = { 0, 0, 0 };
	pd.hashTable = HashTable(hashSize, tmp);
	
	ThreadGroup threads;
	for (int i = 0; i < threadCount; i++)
		threads.create(threadFunc, &pd);
	threads.joinAll();
	
	return pd.nodeCount;
}

} // namespace UnitTest
