/*! \mainpage OmniPerft API documentation
 *
 * \section intro_sec Introduction
 *
 * OmniPerft started with the idea of developing an easy-to-use and flexible
 * Chessboard class which supports all significant 8x8 and 10x8 chess variants.
 * The decision to create a new perft utility came later, which is why the
 * Chess::Board class has a lot more functionality than is required for perft.
 *
 * All public interfaces, and even many internal parts of OmniPerft are
 * thoroughly documented and written with reuse in mind, so don't hesitate
 * to use the code in your own GPL projects.
 *
 * \section inst_sec Installation
 *
 * OmniPerft supports all POSIX platforms and Microsoft Windows. It has three
 * makefiles - one for *nix, one for MinGW on Windows, and one for the
 * MSVC compiler on Windows. No external libraries are required.
 *
 * \author Ilari Pihlajisto (ilari.pihlajisto@mbnet.fi)
 */

#ifndef OMNIPERFT_H
#define OMNIPERFT_H

// ISO C++ doesn't have a 64-bit integer type, so we'll have to
// rely on platform and compiler-specific implementations.
#ifdef _MSC_VER
  typedef unsigned __int64 uint64_t;
#else
  #include <stdint.h>
#endif

#endif // OMNIPERFT
