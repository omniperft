#ifndef THREAD_H
#define THREAD_H

// Find out if we're running any type of the Windows OS.
// If not, we'll assume that the OS uses POSIX.
#if defined(_WIN32) || defined(_WIN64) || defined(__NT__)
  #ifndef WINDOWS
    #define WINDOWS
  #endif
#endif

#ifdef WINDOWS
  #include <windows.h>
  // The return type of the thread function
  #define tfunc_t DWORD WINAPI
#else
  #include <pthread.h>
  // The return type of the thread function
  #define tfunc_t void*
#endif

#include <vector>


/*!
 * \brief A simple class wrapper for Windows and POSIX threads.
 *
 * Only a bare minimum of the threading functionality is supported,
 * so it's not a good idea use this class for any complex SMP stuff.
 *
 * Instances of this class are non-copyable.
 *
 * \sa Mutex
 * \sa MutexLocker
 */
class Thread
{
	public:
		#ifdef WINDOWS
		  /*! Native thread function pointer on Windows. */
		  typedef LPTHREAD_START_ROUTINE func_ptr;
		#else
		  /*! Native thread function pointer on POSIX. */
		  typedef void *(*func_ptr)(void*);
		#endif
		
		#ifdef WINDOWS
		  /*! Native thread handle on Windows. */
		  typedef HANDLE thread_t;
		#else
		  /*! Native thread handle on POSIX. */
		  typedef pthread_t thread_t;
		#endif
		
		/*!
		 * Creates and starts a new thread.
		 *
		 * \param func The function the thread starts in.
		 * \param arg The sole argument for \a func.
		 */
		Thread(func_ptr func, void* arg);
		
		/*! Destructs and joins the thread. */
		~Thread();
		
		/*! Waits until the thread has finished. */
		void join();
		
		/*!
		 * The number of CPUs or cores available.
		 * Returns 0 if it can't be detected.
		 */
		static int cpuCount();
	
	private:
		// Forbid copying
		Thread(const Thread&);
		Thread(Thread&);
		Thread& operator=(const Thread&);
		
		thread_t m_handle;
};

/*!
 * \brief A simple class wrapper for mutex variables.
 *
 * A mutex provides a way to prevent multiple threads from using the
 * same resources at the same time.
 * The easiest and safest way to use the Mutex class is via a MutexLocker.
 *
 * Instances of this class are non-copyable.
 *
 * \sa MutexLocker
 */
class Mutex
{
	public:
		#ifdef WINDOWS
		  /*! Native mutex variable on Windows. */
		  typedef CRITICAL_SECTION mutex_t;
		#else
		  /*! Native mutex variable on POSIX. */
		  typedef pthread_mutex_t mutex_t;
		#endif
		
		/*! Creates and initializes a new Mutex object. */
		Mutex();
		
		/*!
		 * Destroys the mutex.
		 *
		 * \warning
		 * If the mutex is still locked, bad things may happen.
		 * So users of this class should always unlock the mutex
		 * before it's destroyed.
		 */
		~Mutex();
		
		/*! Locks the mutex. */
		void lock();
		
		/*!
		 * Unlocks the mutex.
		 *
		 * \warning The mutex must not be already unlocked.
		 */
		void unlock();
	
	private:
		// Forbid copying
		Mutex(const Mutex& );
		Mutex(Mutex& );
		Mutex& operator=(const Mutex& );
		
		mutex_t m_mutex;
};

/*!
 * \brief A simple class for locking and unlocking mutexes.
 *
 * MutexLocker provides RAII-style locking of a mutex.
 * On construction, it acquires ownership of a mutex and locks it,
 * and on destruction it unlocks and releases the mutex.
 *
 * Instances of this class are non-copyable.
 *
 * \sa Mutex
 */
class MutexLocker
{
	public:
		/*! Creates a new MutexLocker and locks \a mutex. */
		explicit MutexLocker(Mutex& mutex);
		
		/*! Unlocks the mutex and destroys the MutexLocker. */
		~MutexLocker();
	
	private:
		// Forbid copying
		MutexLocker(const MutexLocker&);
		MutexLocker(MutexLocker&);
		MutexLocker& operator=(const MutexLocker&);
		
		Mutex& m_mutex;
};

/*!
 * \brief A container for closely related threads.
 *
 * ThreadGroup is a simple way to manage a collection of threads.
 * New threads can be added to the group, and the whole group can
 * be easily joined.
 *
 * Instances of this class are non-copyable.
 *
 * \sa Thread
 */
class ThreadGroup
{
	public:
		/*! Creates a new ThreadGroup object. */
		ThreadGroup();
		
		/*!
		 * Joins and deletes all running threads in the group,
		 * and finally destroys the ThreadGroup object.
		 */
		~ThreadGroup();
		
		/*!
		 * Creates a new thread, adds it to the group,
		 * starts it, and returns it.
		 *
		 * \param func The function the thread starts in.
		 * \param arg The argument for \a func.
		 * \return Pointer to the new thread.
		 */
		Thread* create(Thread::func_ptr func, void* arg);
		
		/*! Joins and deletes all running threads in the group. */
		void joinAll();
	
	private:
		// Forbid copying
		ThreadGroup(const ThreadGroup&);
		ThreadGroup(ThreadGroup&);
		ThreadGroup& operator=(const ThreadGroup&);
		
		std::vector<Thread*> m_threads;
};


#endif // THREAD
