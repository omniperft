# Unix/Linux Makefile for Omniperft

# set up compiler and options
CC = g++
CFLAGS = -O3
LDFLAGS = -lpthread
EXECUTABLE = omniperft

OBJS = src/chessboard.o src/chessmove.o src/fen.o src/main.o src/movegen.o \
       src/movestring.o src/notation.o src/perft.o src/thread.o src/zobrist.o

.cpp.o:
	$(CC) -c -Wall -pedantic $(CFLAGS) -o $@ $<

all: $(OBJS)
	$(CC) $(OBJS) -o $(EXECUTABLE) $(LDFLAGS)

doc-api:
	doxygen api_docs/Doxyfile

clean:
	rm -f $(OBJS)

clobber: clean
	rm -f $(EXECUTABLE)

# End Makefile
